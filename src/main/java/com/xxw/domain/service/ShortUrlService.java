package com.xxw.domain.service;

public interface ShortUrlService {
    String shortUrl(String url);
}
