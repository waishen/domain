package com.xxw.domain.service;

/**
 * @Description: TODO
 * @author: xiexiaowai
 * @date: 2021年10月11日 15:01
 */
public interface LongUrlService {

    String longUrl(String url);
}
